from django.forms import widgets
from rest_framework import serializers
from polls.models import Poll, Choice

class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice

class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll

    choice_set = ChoiceSerializer(many=True)
