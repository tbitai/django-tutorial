from django.conf.urls import patterns, url
from polls import views

urlpatterns = patterns('',
    # ex: api/polls/5/vote/
    url(r'^(?P<poll_id>\d+)/vote/$', views.vote_api),
    url(r'^latest/$', views.latest_api),
)
