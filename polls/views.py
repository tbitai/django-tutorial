from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
#from django.http import Http404
#from django.template import RequestContext, loader
from django.core.urlresolvers import reverse
from django.views import generic
from polls.models import Poll, Choice
from django.utils import timezone

from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from polls.serializers import PollSerializer

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)



class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_poll_list'

    def get_queryset(self):
        """
        Return the last five published polls
        (not including those set to be published in the future).
        """
        return Poll.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model=Poll
    template_name = 'polls/detail.html'
    
    def get_queryset(self):
        """
        Excludes any polls that aren't published yet.
        """
        return Poll.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
    model=Poll
    template_name = 'polls/results.html'

def vote(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the poll voting form.
        return render(request, 'polls/detail.html', {
            'poll': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))

@api_view(['POST'])
def vote_api(request,poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    selected_choice = p.choice_set.get(pk=request.POST['choice'])
    selected_choice.votes += 1
    selected_choice.save()
    return HttpResponse("Successfully voted!")

@api_view(['GET'])
def latest_api(request):

    # Latest poll (excluding future polls)
    latest_poll_array = Poll.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:1]
    latest_poll = latest_poll_array[0]

    serializer = PollSerializer(latest_poll)
    return JSONResponse(serializer.data)
