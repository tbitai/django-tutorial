django-tutorial
===============

Django 1.6 tutorial project (https://docs.djangoproject.com/en/1.6/intro/tutorial01/) + two APIs:

* `/api/polls/latest` serves the latest poll with its choices as a JSON object.
* you can vote at `/api/polls/<poll_id>/vote` with `POST['choice'=<choice_id>]`.

Dependencies beyond those indicated in the tutorial
---------------------------------------------------

1. To get the project working, in addition to the project files, you should set up a Postgres database as specified in `DATABASES` of `mysite/settings.py`.

2. For the APIs, you have to install the Django REST framework (http://www.django-rest-framework.org/)
